//
//  FeedViewController.h
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedViewController : UIViewController{
    
    __weak IBOutlet UITableView *feedTableView;
    
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}

@end
