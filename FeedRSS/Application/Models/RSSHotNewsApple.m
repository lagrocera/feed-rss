//
//  RSSHotNewsApple.m
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import "RSSHotNewsApple.h"

// http://www.migueldiazrubio.com/2013/01/08/desarrollo-ios-6-blocks-y-gcd-grand-central-dispatch/#

NSString *const URL = @"http://images.apple.com/main/rss/hotnews/hotnews.rss";

@implementation RSSHotNewsApple{
    
    NSMutableArray *newsMutableArray;
    NSString *currentElement;
    completionBlock completion;
    RSSHotNewsApple *new;
    
}

- (void)rssNewsWithCompletionBlock:(completionBlock)block{
    
    completion = block;
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithURL:[NSURL URLWithString:URL] completionHandler:^(NSData * data, NSURLResponse *response, NSError *error) {
        
        if (!error) {
            
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
            if (httpResp.statusCode == 200) { // [response statusCode]
                
                newsMutableArray = [NSMutableArray array];
                
                NSXMLParser *xmlParser = [[NSXMLParser alloc]initWithData:data];
                xmlParser.delegate = self;
                [xmlParser parse];
                
            }
        }
    }] resume];
}


# pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString*)qualifiedName attributes:(NSDictionary *)attributeDict {
    
    if([elementName isEqualToString:@"item"]) {
        new = [RSSHotNewsApple new];
    }
    
    if([elementName isEqualToString:@"title"] || [elementName isEqualToString:@"pubDate"] || [elementName isEqualToString:@"description"] || [elementName isEqualToString:@"link"]) {
        currentElement = elementName;
    }
    
}

- (void)parser:(NSXMLParser*)parser foundCharacters:(NSString*)string {
 
    if ([currentElement isEqualToString:@"title"]) {
        
        new.title = string;
        
    }else if ([currentElement isEqualToString:@"pubDate"]) {
        
        new.pubDate = string;
        
    }else if ([currentElement isEqualToString:@"description"]) {
        
        new.descriptionNew = string;
        
    }else if ([currentElement isEqualToString:@"link"]) {
        
        new.urlNew = [NSURL URLWithString:string];
    }
    
    currentElement = nil;
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [newsMutableArray addObject:new];
    }
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
    
    if (completion) {
        completion(newsMutableArray.copy);
        completion = nil;
    }
    
}


@end
