//
//  RSSHotNewsApple.h
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^completionBlock)(NSArray *news);

@interface RSSHotNewsApple : NSObject<NSXMLParserDelegate>

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *descriptionNew;
@property (strong, nonatomic) NSString *pubDate;
@property (strong, nonatomic) NSURL *urlNew;

- (void)rssNewsWithCompletionBlock:(completionBlock)block;

@end
