//
//  WebViewViewController.m
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()

@end

@implementation WebViewViewController{
    
    NSURL *urlNew;
}

- (instancetype)initWithURL:(NSURL *)url{
    
    if(self = [super init]){
        
        urlNew = url;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [webView loadRequest:[NSURLRequest requestWithURL:urlNew]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [activityIndicator stopAnimating];
}


@end
