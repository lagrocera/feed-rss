//
//  WebViewViewController.h
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewViewController : UIViewController{
    
    
    __weak IBOutlet UIWebView *webView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
}

- (instancetype)initWithURL:(NSURL *)url;

@end
