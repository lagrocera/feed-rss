//
//  DetailNewViewController.m
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import "DetailNewViewController.h"
#import "WebViewViewController.h"

@interface DetailNewViewController ()

@end

@implementation DetailNewViewController{
    
    RSSHotNewsApple *currentNew;
    
}

- (instancetype)initWithNew:(RSSHotNewsApple *)new{
    
    if (self = [super init]) {
        
        currentNew = new;
        
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titleLabel.text = currentNew.title;
    pubDateLabel.text = currentNew.pubDate;
    descriptionNewLabel.text = currentNew.descriptionNew;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)goToURL:(id)sender {
    
    WebViewViewController *webViewVC = [[WebViewViewController alloc]initWithURL:currentNew.urlNew];
    
    [self.navigationController pushViewController:webViewVC animated:YES];
    
}
@end
