//
//  DetailNewViewController.h
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSHotNewsApple.h"

@interface DetailNewViewController : UIViewController{
    
    __weak IBOutlet UILabel *titleLabel;
    __weak IBOutlet UILabel *pubDateLabel;
    __weak IBOutlet UILabel *descriptionNewLabel;
    
    
}

- (instancetype)initWithNew:(RSSHotNewsApple *)new;

- (IBAction)goToURL:(id)sender;

@end
