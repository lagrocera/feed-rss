//
//  FeedCell.h
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
