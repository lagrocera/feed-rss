//
//  FeedViewController.m
//  FeedRSS
//
//  Created by Luis F Ruiz Arroyave on 10/18/15.
//  Copyright © 2015 MRuiz. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedCell.h"
#import "RSSHotNewsApple.h"
#import "DetailNewViewController.h"

NSString *const CELL_IDENTIFIER = @"cell";

@interface FeedViewController ()

@end

@implementation FeedViewController{
    
    NSArray *newsData;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Hot News Apple";

    [feedTableView registerNib:[UINib nibWithNibName: NSStringFromClass([FeedCell class]) bundle:nil] forCellReuseIdentifier:CELL_IDENTIFIER];
    
    RSSHotNewsApple *rss = [RSSHotNewsApple new];

    [rss rssNewsWithCompletionBlock:^(NSArray *news) {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            
            newsData = news;
            [feedTableView reloadData];
            [activityIndicator stopAnimating];
            
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return newsData.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FeedCell *cell = [feedTableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    RSSHotNewsApple *rss = newsData[indexPath.row];
    
    cell.titleLabel.text = rss.title;
    cell.dateLabel.text = rss.pubDate;
    
    return cell;
    
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:( NSIndexPath *)indexPath{
    
    RSSHotNewsApple *new = newsData[indexPath.row];
    
    DetailNewViewController *detailVC = [[DetailNewViewController alloc]initWithNew:new];
    
    [self.navigationController pushViewController:detailVC animated:YES];
}


@end
